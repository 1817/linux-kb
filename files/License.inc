<?php

namespace JetBackup\Core;

if (!defined("JBM")) {
    exit("Restricted Access!");
}
class License
{
    private $_localKey = NULL;
    const LICENSE_CHECK_URL = "https://check.jetlicense.com";
    const LICENSE_LOCALKEY_DAYS = 259200;
    const LICENSE_LOCALKEY_CHECK_INTERVAL = 172800;
    const LICENSE_LOCALKEY_FAIL_INTERVAL = 3600;
    const LICENSE_NOTIFY_ERROR_INTERVAL = 86400;
    const LICENSE_PRODUCT_ID = "58ac64be19a4643cdf582727";
    const LICENSE_PUBLIC_KEY = "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2F105dMYw8kTC54ANpMt\nWaVKEFEoRd+D0YDRDB/EGgFbEUdkDvub6FfHcDdgfmRgjUOWap0yX2MpOCy/QA/P\nSrIuSYe4+jv5J+cW2O6WbnhVdlAQTLOiOgiUYMuFNt6+gx4DaitoxD/p39elThfa\nKsAawrrxBR0mEdn0JeE1k0l/bbhekzUubR9LReOtveXdMoVygrAQ52mQ0saOSmuB\nUn2oa4kaWeZfCA/fYW8jMEEjmrftcT3XVi7yA5Fk/xQQ3eNSLIAx2gZf8B2g0zqa\n/QSq9+6BDM/6OdG9o+cMvpSoOojx5ApcQxAwHfSiISfibJ/49kmnP7okCRI7pNvE\n3wIDAQAB\n-----END PUBLIC KEY-----";
    public function __construct()
    {
    }
    /**
     * @return string
     * @throws \Exception
     */
    private function _handshake()
    {
        if (file_exists("/sys/class/dmi/id/product_uuid")) {
            $code = Shell::exec("/bin/cat /sys/class/dmi/id/product_uuid", $response);
            if (!$code && isset($response[0])) {
                return md5($response[0]);
            }
        }
        if (file_exists("/var/lib/dbus/machine-id")) {
            $code = Shell::exec("/bin/cat /var/lib/dbus/machine-id", $response);
            if (!$code && isset($response[0])) {
                return md5($response[0]);
            }
        }
        $code = Shell::exec("/sbin/ifconfig -a | /bin/grep -i 'hwaddr' | /bin/awk ' { print \$5 } ' | /usr/bin/head -n1", $response);
        if (!$code && isset($response[0])) {
            return md5($response[0]);
        }
        $code = Shell::exec("/sbin/ip addr | /bin/grep -i 'ether' | /bin/awk  ' { print \$2 } ' | /usr/bin/head -n1", $response);
        if (!$code && isset($response[0])) {
            return md5($response[0]);
        }
        $code = Shell::exec("/sbin/ifconfig -a | /bin/grep -i 'ether' | /bin/awk ' { print \$2 } ' | /usr/bin/head -n1", $response);
        if (!$code && isset($response[0])) {
            return md5($response[0]);
        }
        if (file_exists("/etc/machine-id")) {
            $code = Shell::exec("/bin/cat /etc/machine-id", $response);
            if (!$code && isset($response[0])) {
                return md5($response[0]);
            }
        }
        return "";
    }
    /**
     * @param string $error
     * @param bool $force
     */
    private static function _checkFailed($error, $force = false)
    {
        $license = Factory::getConfigLicense();
        if (Factory::getConfigNotification()->getGlobal() && ($force || $license->getNotifyDate() < time() - 60 * 60 * 6)) {
            $license->setNotifyDate();
            $license->update();
            $hostname = Utils::getHostname();
            Factory::getConfigNotification()->sendMail("License check FAILED on " . $hostname, "There was a failed license check on the server " . $hostname . "<br />Error: " . $error);
        }
    }
    /**
     * @return array
     */
    private function _parseLocalKey()
    {
        $output = array("signed" => "", "signed_status" => "", "status" => "", "description" => "");
        if (!$this->_localKey) {
            return $output;
        }
        list($signed, $signed_status, $status, $description) = explode("|", $this->_localKey, 4);
        if ($signed) {
            $output["signed"] = $signed;
        }
        if ($signed_status) {
            $output["signed_status"] = $signed_status;
        }
        if ($status) {
            $output["status"] = $status;
        }
        if ($description) {
            $output["description"] = $description;
        }
        return $output;
    }
    /**
     * @param bool $reset
     * @param string $message
     */
    private static function _handleNotify($reset = false, $message = "")
    {
        $license = Factory::getConfigLicense();
        $update_time = null;
        if ($reset) {
            $update_time = 0;
        } else {
            if (!$license->getNotifyDate()) {
                $update_time = time() + self::LICENSE_NOTIFY_ERROR_INTERVAL;
            } else {
                if ($license->getNotifyDate() < time()) {
                    self::_checkFailed($message, true);
                    $update_time = 0;
                }
            }
        }
        if ($update_time != null) {
            $license->setNotifyDate($update_time);
            $license->update();
        }
    }
    /**
     * @throws \Exception
     */
    public function retrieveLocalKey()
    {
        $license = Factory::getConfigLicense();
        if (time() < $license->getNextCheck()) {
            return NULL;
        }
        $license->setNextCheck(time() + self::LICENSE_LOCALKEY_FAIL_INTERVAL);
        $license->update();
        $this->_localKey = $license->getLocalKey();
        $localkey_details = $this->_parseLocalKey();
        $handshake = $this->_handshake();
        if (!$handshake) {
            $message = "Unable to get server handshake key";
            self::_checkFailed($message);
            $license->setLocalKey((string) $localkey_details["signed"] . "|" . $localkey_details["signed_status"] . "|Invalid|" . $message);
            $license->update();
        } else {
            $public_key = openssl_get_publickey(self::LICENSE_PUBLIC_KEY);
            $handshake_encrypted = "";
            if (!openssl_public_encrypt($handshake, $handshake_encrypted, $public_key)) {
                $message = "Unable to prepare handshake key for validation";
                self::_checkFailed($message);
                $license->setLocalKey((string) $localkey_details["signed"] . "|" . $localkey_details["signed_status"] . "|Invalid|" . $message);
                $license->update();
            } else {
                Shell::exec("hostname", $hostname);
                Shell::exec("uname -r", $kernel_release);
                Shell::exec("uname -v", $kernel_version);
                $postfields = array("output" => "json", "product_id" => self::LICENSE_PRODUCT_ID, "handshake" => base64_encode($handshake_encrypted), "info" => array("Hostname" => isset($hostname[0]) ? trim($hostname[0]) : "", "Product_Version" => trim(Utils::getVersion()), "Product_Tier" => trim(Utils::getTier()), "Operating_System" => file_exists("/etc/redhat-release") ? trim(file_get_contents("/etc/redhat-release")) : "", "Kernel_Release" => isset($kernel_release[0]) ? trim($kernel_release[0]) : "", "Kernel_Version" => isset($kernel_version[0]) ? trim($kernel_version[0]) : "", "cPanel_Version" => file_exists("/usr/local/cpanel/version") ? trim(file_get_contents("/usr/local/cpanel/version")) : ""));
                $curl = new \nss\core\web\UrlRequest(self::LICENSE_CHECK_URL, $postfields);
                $curl->sendRequest();
                if (!($data = $curl->getContents())) {
                    $message = "Could not resolve host (" . self::LICENSE_CHECK_URL . ")";
                    self::_handleNotify(false, $message);
                    $license->setLocalKey((string) $localkey_details["signed"] . "|" . $localkey_details["signed_status"] . "|Invalid|" . $message);
                    $license->update();
                } else {
                    $result = json_decode($data, true);
                    if ($result === false) {
                        $message = "No valid response received from the license server";
                        self::_handleNotify(false, $message);
                        $license->setLocalKey((string) $localkey_details["signed"] . "|" . $localkey_details["signed_status"] . "|Invalid|" . $message);
                        $license->update();
                    } else {
                        if (!$result["success"] || !$result["data"]["localkey"]) {
                            $message = "Invalid response from licensing server: " . $result["message"];
                            self::_checkFailed($message);
                            $license->setLocalKey((string) $localkey_details["signed"] . "|" . $localkey_details["signed_status"] . "|Invalid|" . $message);
                            $license->update();
                        } else {
                            $this->_localKey = $result["data"]["localkey"];
                            $new_localkey_details = $this->_parseLocalKey();
                            $signed = base64_decode($new_localkey_details["signed"]);
                            $signed_key = sha1(intval(time() / self::LICENSE_LOCALKEY_DAYS) . $handshake, true);
                            if (!openssl_verify($signed_key, $signed, $public_key, OPENSSL_ALGO_SHA256)) {
                                $message = "Your handshake key was changed on your server.<br />Please reissue your license through your JetApps licenses distributor.<br /><a href='https://www.jetapps.com/authorized-distributors' target='_blank'>Click here to view list of authorized distributors</a><br /><br />After reissue your license, please try to run the following command from the server cli:<br />jetcli backup -R license";
                                self::_checkFailed($message);
                                $license->setLocalKey((string) $localkey_details["signed"] . "|" . $localkey_details["signed_status"] . "|Invalid|" . $message);
                                $license->update();
                            } else {
                                self::_handleNotify(true);
                                $license->setLocalKey($this->_localKey);
                                $license->setLastCheck();
                                $license->setNextCheck(time() + self::LICENSE_LOCALKEY_CHECK_INTERVAL);
                                $license->update();
                            }
                        }
                    }
                }
            }
        }
    }
    /**
     * @return array
     * @throws \Exception
     */
    public function verifyLocalKey()
    {
        $output = array("active" => false, "status" => "Invalid", "description" => "");
        $license = Factory::getConfigLicense();
        $this->_localKey = $license->getLocalKey();
        if (!$this->_localKey) {
            $output["description"] = "Unable to parse license localkey";
            return $output;
        }
        $handshake = $this->_handshake();
        if (!$handshake) {
            $output["description"] = "Unable to get server handshake";
            return $output;
        }
        $localKeyDetails = $this->_parseLocalKey();
        if ($localKeyDetails["status"]) {
            $output["status"] = $localKeyDetails["status"];
        }
        if ($localKeyDetails["description"]) {
            $output["description"] = $localKeyDetails["description"];
        }
        $public_key = openssl_get_publickey(self::LICENSE_PUBLIC_KEY);
        $modulo = $license->getLastCheck() % self::LICENSE_LOCALKEY_DAYS;
        $signed = base64_decode($localKeyDetails["signed_status"]);
        $signed_key = sha1(intval((time() - $modulo) / self::LICENSE_LOCALKEY_DAYS) . $handshake . "Active", true);
        if (!openssl_verify($signed_key, $signed, $public_key, OPENSSL_ALGO_SHA256)) {
            if ($output["status"] == "Active" || $output["status"] == "Trial" || $output["status"] == "Invalid" && !$output["description"]) {
                $output["status"] = "Invalid";
                $output["description"] = "Automatic license refresh is in progress, Please try again in couple of minutes.";
                $license->setLocalKey((string) $localKeyDetails["signed"] . "|" . $localKeyDetails["signed_status"] . "|Invalid|" . $output["description"]);
                $license->setNextCheck(0);
                $license->update();
            }
			
			$output["status"] == "Active";
            return $output;
        }
        $output["active"] = true;
        return $output;
    }
    public static function check()
    {
        static $license = NULL;
        if (!$license) {
            $license = new License();
        }
        try {
            $license->retrieveLocalKey();
//          return $license->verifyLocalKey();
            return $output["status"] = "Active";
        } catch (\Exception $e) {
            return false;
        }
    }
}

?>